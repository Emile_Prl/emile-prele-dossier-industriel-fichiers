# -*- coding: utf-8 -*-
"""
Created on Mon May 20 10:19:12 2024

@author: eprel
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftshift
from matplotlib.patches import Rectangle
from matplotlib.gridspec import GridSpec
from matplotlib.lines import Line2D
plt.rcParams["font.family"] = "Times New Roman"

def read_xmot(nom):
    with open(nom+".txt", "r") as f:
        lignes = f.readlines()
    Xphasemot = []

    for ligne in lignes:
        valeurs_ligne = ligne.split(",")
        for valeur in valeurs_ligne:
            Xphasemot.append(float(valeur))
            
    return Xphasemot

Vphase = np.array(read_xmot("Vphasemot_simu"))/10
Iphase = read_xmot("Iphasemot_simu")

fe = 100000

Time = np.linspace(0,len(Vphase)/fe,len(Vphase))

tplot = 1
nplot = int(tplot*fe)
fig1 = plt.figure(figsize=(7, 4), dpi=200)
ax1 = fig1.add_subplot(211)
ax1.plot(Time[:nplot]*1e3, Vphase[:nplot], '-', color = 'tab:blue', linewidth=1)
ax1.set_ylabel('Tension simple 1 (V)')

plt.setp(ax1.get_xticklabels(), visible=False)

ax2 = fig1.add_subplot(212)
ax2.plot(Time[:nplot]*1e3, Iphase[:nplot], '-', color='tab:orange', linewidth=1)
ax2.set_ylabel('Courant de phase 1 (A)')
ax2.set_xlabel('Temps (ms)')

plt.savefig('plot_pleine_onde.jpg',bbox_inches='tight')
plt.savefig('plot_pleine_onde.pdf',bbox_inches='tight')


plt.show()