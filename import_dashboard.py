# -*- coding: utf-8 -*-
"""
Created on Mon May 20 10:54:23 2024

@author: eprel
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftshift
from matplotlib.patches import Rectangle
from matplotlib.gridspec import GridSpec
from matplotlib.lines import Line2D
import pandas as pd
plt.rcParams["font.family"] = "Times New Roman"


fichier_csv = 'mesure_courant_sinus.csv'

# Lire le fichier CSV
df = pd.read_csv(fichier_csv)
df = df.iloc[1:, :]

data = df.to_numpy()

y1 = data[:,0]

fe = 2000
x1 = np.linspace(0,len(y1)/fe,len(y1))

ymin = min(y1)
ymax = max(y1)


fig = plt.figure(figsize=(9, 4),dpi=200)
ax1 = fig.add_subplot(211)

tplot1 = 0
deltaT = 0.5

nplot1 = int(tplot1*fe)
nplot2 = int((tplot1+deltaT)*fe)

ax1.plot(x1[nplot1:nplot2],y1[nplot1:nplot2],linewidth=1)
ax1.axhline(y=0.5,color='red',linestyle='--')
ax1.set_ylabel('Tension de mesure (V)')
ax11 = ax1.twinx()
ax11.set_ylim(ymin*10,ymax*10)
ax11.set_ylabel('Courant (A)')

ax2 = fig.add_subplot(212)

tplot1 = 17.3

nplot1 = int(tplot1*fe)
nplot2 = int((tplot1+deltaT)*fe)

ax2.plot(x1[nplot1:nplot2],y1[nplot1:nplot2],linewidth=1)
ax2.axhline(y=0.5,color='red',linestyle='--')
ax2.set_ylabel('Tension (V)')
ax2.set_xlabel('Temps (s)')
ax22 = ax2.twinx()
ax22.set_ylim(ymin*10,ymax*10)
ax22.set_ylabel('Courant (A)')

plt.savefig('plot_mesure_sinus.jpg',bbox_inches='tight')
plt.savefig('plot_mesure_sinus.pdf',bbox_inches='tight')

plt.show()
