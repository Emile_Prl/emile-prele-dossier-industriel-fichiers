#include "LowPower.h"

const int Switch_armement = 50;   // Broche de l'interrupteur
const int Switch_record = 48;   // Broche de l'interrupteur
const int led_arme = 30;    // Broche de la LED
const int led_record = 32;    // Broche de la LED
const int courantPin = A0; // Broche analogique du premier capteur de courant
const int record_cmd = 10;
const int record_cmd2 = 9;
const int alim_boost = 44;
const int alim_pince2 = 40;
const int alim_dataq = 42;

int etatSwitch_record = 0;        // Variable pour stocker l'état du Switch
int etatSwitch_armement = 0;        // Variable pour stocker l'état du Switch

float seuilCourant = 3; // Seuil de courant en ampères
float calibre_sonde = 0.1; // en V/A

void setup() {
  Serial.begin(9600);
  Serial.println("---------------------------------");
  pinMode(Switch_armement, INPUT);
  pinMode(Switch_record, INPUT);
  pinMode(led_record, OUTPUT);
  pinMode(led_arme, OUTPUT);
  pinMode(alim_boost, OUTPUT);
  pinMode(alim_pince2, OUTPUT);
  pinMode(alim_dataq, OUTPUT);
  pinMode(record_cmd,OUTPUT);
  pinMode(record_cmd2,OUTPUT);
}

void loop() {
 digitalWrite(alim_dataq, HIGH);
 digitalWrite(alim_boost, HIGH);
 digitalWrite(alim_pince2, HIGH);
 // Vérifie si le Switch est enfoncé
 etatSwitch_armement = digitalRead(Switch_armement);
 etatSwitch_record = digitalRead(Switch_record);

 // Si le Switch est enfoncé
 if (etatSwitch_record == HIGH){
  delay(1000);
  digitalWrite(led_record, HIGH);
  delay(10);
  digitalWrite(alim_boost, LOW);
  delay(1000);
  digitalWrite(alim_dataq, LOW);
  start_record();
  int cond = HIGH;
  while (cond == HIGH){
    //lowPowerSleep(60); en s
    delay(100);
    cond = digitalRead(Switch_record);
  }
  stop_record();
  delay(2000);
  digitalWrite(alim_dataq, HIGH);
  digitalWrite(alim_pince2, HIGH);
  digitalWrite(alim_boost, HIGH);
 }
  else if (etatSwitch_armement == HIGH and etatSwitch_record == LOW) {

    digitalWrite(led_arme,HIGH);
    digitalWrite(alim_dataq, HIGH);
    digitalWrite(alim_pince2, HIGH);
    digitalWrite(alim_boost, LOW);

    // Calcule la moyenne des deux mesures de courant
    float rmsCourant = RMS_courant();

    // Vérifie si la mesure de courant est inférieure au seuil
    if (rmsCourant > seuilCourant) {
      digitalWrite(alim_dataq, LOW);
      delay(5000);
      start_record();
      delay(6000); // 1 min
      //lowPowerSleep(60); en s
      float cond = rmsCourant;
      while (rmsCourant > seuilCourant){
        delay(10000); //1 min
        //lowPowerSleep(60); en s
        rmsCourant = RMS_courant();
      }
      stop_record();
    }
    digitalWrite(alim_dataq, HIGH);
    digitalWrite(alim_pince2, HIGH);
    digitalWrite(alim_boost, HIGH);
    delay(5000);// 5min
    //lowPowerSleep(300); en s
  }

  else if (etatSwitch_armement == LOW and etatSwitch_record == LOW) {
    digitalWrite(led_arme, LOW);
    digitalWrite(led_record, LOW);
    digitalWrite(alim_dataq, HIGH);
    digitalWrite(alim_pince2, HIGH);
    digitalWrite(alim_boost, HIGH);
  }
}

void start_record(){
  digitalWrite(led_arme, LOW);
  digitalWrite(led_record,HIGH);
  digitalWrite(record_cmd,HIGH);
  digitalWrite(record_cmd2,HIGH);
}

void stop_record(){
  digitalWrite(led_record,LOW);
  digitalWrite(record_cmd,LOW);
  digitalWrite(record_cmd2,LOW); 
}


//float RMS_courant () {
//  float sum_courant_rms = 0;
//  float i1 = 0;
//  float i2 = 0;
//  float prod0 = 1;
//  float prod1 = 1;
//  float rms = 0;
//  float T = 0;
//  float f = 0;
//  int courantPin = A0; // Assuming courantPin is analog pin A0
//
//  delay(100);
//
//  // Initial read to establish a baseline current value
//  float i0 = analogRead(courantPin) - 512;
//  int i = 0;
//
//  // Find the first zero crossing
//  while (prod0 > 0) {
//    i1 = analogRead(courantPin) - 512; // Convert value to current
//    prod0 = i1 * i0;
//  }
//
//  unsigned long StartTime = millis();
//  unsigned long CurrentTime;
//  i = 0;
//  int zeroCrossings = 0;
//
//  // Find the zero crossings for five periods (six zero crossings)
//  while (zeroCrossings < 11) {
//    i2 = analogRead(courantPin) - 512; // Convert value to current
//    prod1 = i1 * i2;
//    if (prod1 < 0) { // Detect zero crossing
//      zeroCrossings++;
//      if (zeroCrossings == 1) {
//        StartTime = millis(); // Time at the first zero crossing
//      }
//      if (zeroCrossings == 11) {
//        CurrentTime = millis(); // Time at the sixth zero crossing
//      }
//    }
//    i1 = i2;
//  }
//
//  T = 2*(CurrentTime - StartTime) / 10.0; // Average period of one cycle
//  f = 1000.0 / T; // Frequency in Hz
//  int Te = T / 10; // Sampling interval in milliseconds
//  Serial.println(f);
//
//  // Calculate RMS current over 100 samples
//  float mesure_courant = 0;
//  for (int i = 0; i < 100; i++) {
//    mesure_courant = analogRead(courantPin) - 512; // Convert value to current
//    delay(Te);
//    sum_courant_rms += pow(mesure_courant, 2); // Sum of squares for RMS
//  }
//
//  rms = 2 * sqrt(sum_courant_rms / 100) * 5 / 1024; // Convert to Amperes
//  Serial.println(rms/2);
//  return rms;
//}



float RMS_courant (){
  float sum_courant_rms=0;
  float sum_courant_mean=0;
  float measure_courant=0;
  float mean=0;
  float rms=0;
  delay(100);
  for (int i=1; i<1000; i++){
    measure_courant = analogRead(courantPin); // Convertit la valeur en courant
    //Serial.println(measure_courant);
    delay(1);
    sum_courant_mean = sum_courant_mean + measure_courant; //mean
  }
  mean = sum_courant_mean/1000;
  for (int i=1; i<1000; i++){
    measure_courant = analogRead(courantPin)-mean; // Convertit la valeur en courant
    delay(1);
    sum_courant_rms = sum_courant_rms + pow(measure_courant,2); //RMS
  }
  rms = 2*sqrt(sum_courant_rms/1000)*5/(1024)/calibre_sonde; // en A
  Serial.println(rms);
  
  return rms;
}

void lowPowerSleep(int seconds)
{
  int sleeps = seconds / 8;
  for (int i = 0 ; i < sleeps ; i++) {
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
  }
}
