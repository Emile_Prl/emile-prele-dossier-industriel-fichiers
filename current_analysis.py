# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 09:08:10 2024

@author: eprel
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftshift
from matplotlib.patches import Rectangle
from matplotlib.gridspec import GridSpec
from matplotlib.lines import Line2D
import pandas as pd
plt.rcParams["font.family"] = "Times New Roman"

def read_Imot(nom):
    df = pd.read_csv(nom)
    df = df.iloc[1:, :]

    data = df.to_numpy()

    y1 = data[:,0]
            
    return y1

x1 = read_Imot("record_sans_defaut.csv")
x2 = read_Imot("record_avec_defaut.csv")

x2 = x2*1.5
temps = int(1e4)
dt = 3e3

# Paramètres du signal
V = 300
T = 1/50

#%%

fs = 40e3

t1 = np.linspace(0,len(x1)/fs,len(x1))
t2 = np.linspace(0,len(x2)/fs,len(x2))

tplot = 5
dtplot = 0.3 
nplot1 = int(5*fs)
nplot2 = int((dtplot+tplot)*fs)

plt.figure(figsize=[8,2],dpi=200)
plt.plot(t1[nplot1:nplot2],x1[nplot1:nplot2])
plt.plot(t2[nplot1:nplot2],-x2[nplot1:nplot2])
plt.show()

def calc_fft(x,fs):
    # Transformation de Fourier du signal
    fft1 = fft(x)
    fft1_shift = fftshift(fft1)
    n = len(x)
    
    f = np.arange(n) * (fs / n)     # Plage de fréquences
    fshift = np.arange(-n/2, n/2) * (fs / n)  # Plage de fréquences centrée sur zéro
    return [fshift, fft1_shift,n]

[fshift1, fft1_shift, n1] = calc_fft(x1, fs)
[fshift2, fft2_shift, n2] = calc_fft(x2, fs)

def fft_zoom(fshift1, fft1_shift, f1max,f2min,f2max, n1,fs):
    fplotmax1_large = n1/2+n1/(fs/f1max)
    fplotmin1_zoom = n1/2+n1/(fs/f2min)
    fplotmax1_zoom = n1/2+n1/(fs/f2max)
    
    fshift1_large = fshift1[int(n1/2):int(fplotmax1_large)]
    fft1_large = 10*np.log10(np.abs(fft1_shift[int(n1/2):int(fplotmax1_large)])/n1)
    fshift1_zoom = fshift1[int(fplotmin1_zoom):int(fplotmax1_zoom)]
    fft1_zoom = 10*np.log10(np.abs(fft1_shift[int(fplotmin1_zoom):int(fplotmax1_zoom)])/n1)
    return [fshift1_large, fft1_large, fshift1_zoom, fft1_zoom]

def zoomingBox(ax1, roi, ax2, color, linewidth=2):
    ax1.add_patch(Rectangle([roi[0],roi[2]], roi[1]-roi[0], roi[3]-roi[2],**dict([('fill',False), ('linestyle','dashed'), ('color',color), ('linewidth',linewidth)]) ))
    srcCorners = [[roi[0],roi[2]], [roi[0],roi[3]], [roi[1],roi[2]], [roi[1],roi[3]]]
    dstCorners = ax2.get_position().corners()
    
    ax1.annotate('',
            xy=(roi[0], roi[2]), xycoords='data',
            xytext=(228, -101), textcoords='data',
            arrowprops=dict([('arrowstyle','-'),('color',color),('linewidth',linewidth)]))
    ax1.annotate('',
            xy=(roi[1], roi[2]), xycoords='data',
            xytext=(1780, -100), textcoords='data',
            arrowprops=dict([('arrowstyle','-'),('color',color),('linewidth',linewidth)]))
   

def plot_fft_current(fshift1, fft1_shift,fshift2, fft2_shift, f1max,f2min,f2max, n1,n2,fs):
    
    [fshift1_large, fft1_large, fshift1_zoom, fft1_zoom] = fft_zoom(fshift1, fft1_shift, f1max, f2min, f2max, n1, fs)
    [fshift2_large, fft2_large, fshift2_zoom, fft2_zoom] = fft_zoom(fshift2, fft2_shift, f1max, f2min, f2max, n2, fs)
    
    fig = plt.figure(dpi=300, figsize=[10,6])
    gs = GridSpec(2,7, height_ratios=[2,2])
    ax1=fig.add_subplot(gs[0,:]) # First row, first column
    ax2=fig.add_subplot(gs[1,1:6]) # First row, second column
    
    #ax1.tick_params(labelbottom=False)
    ax1.plot(fshift1_large,fft1_large, color='tab:blue',linewidth=0.7,label="Sans défaut")
    ax1.plot(fshift2_large,fft2_large, color='tab:orange',linewidth=0.7,label="Avec défaut")
    ax1.set(ylabel="Amplitude (dB)",xlabel="fréquence (Hz)")
    ax1.grid()
    ax1.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
    ax1.yaxis.major.formatter._useMathText = True
    ax1.axvspan(int(n1/(14*fs)*f2min),int(n1/(14*fs)*f2max), color='dodgerblue', alpha=0.2)
    ax1.legend()
    
    ax2.plot(fshift1_zoom,fft1_zoom, color='tab:blue',linewidth=1.5,label="Sans défaut")
    ax2.plot(fshift2_zoom,fft2_zoom, color='tab:orange',linewidth=1.5,label="Avec défaut")
    ax2.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
    ax2.patch.set_facecolor('dodgerblue')
    ax2.patch.set_alpha(0.1)
    ax2.set(ylabel="Amplitude (dB)",xlabel="fréquence (Hz)")

    ax2.legend()
    ax2.grid()
    
    zoomingBox(ax1, [f2min,f2max,-45,10], ax2, 'dodgerblue')
    plt.savefig('plot_fft_current.jpg',bbox_inches='tight')
    plt.savefig('plot_fft_current.pdf',bbox_inches='tight')
    
    plt.show()

plot_fft_current(fshift1, fft1_shift,fshift2, fft2_shift, 2000,40,60,n1,n2,fs)