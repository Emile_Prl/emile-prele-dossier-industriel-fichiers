Vdc = 5000;
L = 10e-3;
R = 0.1;
fe = 100000;


%% Save des données GMSK
Imot = out.imot.Data;
tt = out.imot.Time;
Vmot = out.vmot.Data;
nmin = 270100;
nmax = 390200;
nmax = nmin+3350
Imot1 = Imot(nmin:nmax);
Vmot1 = Vmot(nmin:nmax);

writematrix(Imot1, 'Iphasemot_simu.txt')
writematrix(Vmot1, 'Vphasemot_simu.txt')
Imot2 = Imot1./max(abs(Imot1));
N = length(Imot2);
figure;
subplot(2,1,1)
plot(Imot2)
subplot(2,1,2)
plot(Vmot)

Imot3 = int32(127*Imot2)+127;

figure()
plot(Imot3)

fid_imot=fopen('Imot_GBF.txt','wt');
fprintf(fid_imot,'%d\n',length(Imot3)); % 1ere ligne : nombre de points
fprintf(fid_imot,'%d\n',max(Imot3)); % 2e ligne : valeur max
fprintf(fid_imot,'%d\n',min(Imot3)); % 3e ligne : valeur max
fprintf(fid_imot,'%d\n',Imot3); % 4e ligne -> fin : liste des valeurs
fclose(fid_imot);

%% 
tplot = 
