import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tkinter as tk
from PIL import ImageTk, Image
from tkinter import messagebox
import csv


#Charge_powerbank = 20 #Ah
Arduino_conso_sleep = 6e-3
Arduino_conso_busy = 27e-3

ma200_miniflex = 5.23e-3 # sous 9V

mosfet = 77e-3
led = 6.5e-3
dataq = 160e-3
rendement_boost = 0.93
boost_led = 25.7e-3

Crr = 0.002
Fvmax = 100e3  # N
M = 200e3
Rroue = 1.15/2
g=9.81

Lmu = 200e-3
R2 = 0.3
Rroue = 1.15/2
p=3
v = 100 #km/h
ws = p*v/3.6/Rroue
gliss = 0.05

def couple_roue(alpha_rad):    
    F = M * g * (Crr * np.cos(alpha_rad) + np.sin(alpha_rad)) + Fvmax
    C = F/4
    return C

def Courant_moteur(alpha):
    
    A = 3*2*p*Lmu**2*ws*R2/gliss*1/((R2/gliss)**2+(Lmu*ws)**2)*1.2
    
    if alpha>=0 :
        alpha = np.arctan(alpha)
        Croue = couple_roue(alpha)
        Cmot = 23/109*Croue
        Imot = np.sqrt(Cmot/A)
    else :
        alpha = np.arctan(alpha)
        Croue = abs(couple_roue(alpha))
        Cmot = 23/109*Croue
        Imot = -np.sqrt(Cmot/A)
    return Imot

def autonomie_armed(temps_min,Imot,seuil, Charge_powerbank, Arduino_conso_busy, Arduino_conso_sleep):
    Charge = np.ones((temps_min))*Charge_powerbank    
    Current = np.zeros((temps_min))
    j=0
    i=0
    for i in range(temps_min):
        j=j+1
        if j%5 !=0 :
            Current[i] = (Arduino_conso_sleep+led)
        elif j%5 == 0 :
            Current[i] = (1*(Arduino_conso_busy+mosfet+led+(ma200_miniflex*9/5/rendement_boost)+boost_led)+5*(Arduino_conso_sleep+led))/6
            j=0
    for i in range(temps_min-1):
        if Imot[i]>=seuil:
            Current[i]=(Arduino_conso_busy+led+3*mosfet+dataq+2*(ma200_miniflex*9/5/rendement_boost)+boost_led)
    
    for i in range(1,temps_min):
        Charge[i] = Charge[i-1]-Current[i]/60

    Time = np.linspace(1,temps_min,temps_min)
    
    return [Charge, Current*1000, Time]

def tracer_courbes():
    # Récupérer les noms des gares de départ et d'arrivée
    trajet = choix_trajet.get()
    Charge_powerbank = float(charge_powerbank.get())

    # Ouvrir le fichier CSV en mode lecture
    nom_fichier = f'plot-data_{trajet}.csv'
    try:
        with open(nom_fichier, mode='r') as fichier_csv:
            lecteur_csv = csv.reader(fichier_csv)
            donnees_liste = list(lecteur_csv)
    except FileNotFoundError:
        messagebox.showerror("Erreur", f"Le fichier {nom_fichier} n'a pas été trouvé.")
        return

    # Convertir la liste en un tableau NumPy
    profil_alt = np.array([[float(valeur) for valeur in ligne] for ligne in donnees_liste[1:]])

    profil_alt_x = profil_alt[:, 0]
    profil_alt_y = profil_alt[:, 1]
    
    nb_points_interpolation = 100
    seuil_courant = 250
    vitesse = 100 #km/h
    distance = max(profil_alt_x) - min(profil_alt_x)
    temps_min = int(distance/(vitesse*1000/60))

    # Création de nouveaux points X pour l'interpolation
    x_interp = np.linspace(0, distance, temps_min)

    # Interpolation des valeurs Y
    y_interp = np.interp(x_interp, profil_alt_x, profil_alt_y)

    pentes = np.diff(y_interp) / np.diff(x_interp) * 100
    
    Imot = [Courant_moteur(pentei) for pentei in pentes]
    

    [Charge, Courant, Temps] = autonomie_armed(temps_min, Imot, seuil_courant, Charge_powerbank, Arduino_conso_busy, Arduino_conso_sleep)
    
    plt.figure(dpi=100)
    plt.plot(x_interp[:-1]/1000, pentes*1e4)
    plt.plot(x_interp[:-1]/1000, Imot)
    plt.show()
    
    # Tracer les courbes dans la fenêtre Tkinter
    fig1 = plt.figure(figsize=(6, 5), dpi=100)
    ax1 = fig1.add_subplot(211)
    ax1.plot(x_interp/1000, y_interp, '-')
    ax1.set_title(f'Profil altimétrique sur {trajet}')
    ax1.set_ylabel('Altitude (m)')
    ax1.grid()
    plt.setp(ax1.get_xticklabels(), visible=False)

    ax2 = fig1.add_subplot(212)
    ax2.plot(x_interp[:-1]/1000, Imot, '-')
    ax2.axhline(y=seuil_courant,color='r',linestyle='--')
    ax2.set_title(f'Courant moteur sur {trajet}')
    ax2.set_xlabel('Distance (km)')
    ax2.set_ylabel('Courant moteur (A)')
    ax2.grid()
    
    fig2 = plt.figure(figsize=(6, 5), dpi=100)
    ax1 = fig2.add_subplot(211)
    ax1.plot(Temps, Courant, '.-')
    ax1.set_title(f'Consommation de courant du Datalogger')
    ax1.set_ylabel('Courant (mA)')
    plt.setp(ax1.get_xticklabels(), visible=False)
    ax1.grid()
    
    ax2 = fig2.add_subplot(212)
    ax2.plot(Temps, Charge*100/Charge_powerbank, '-')
    ax2.set_title(f'Charge du Datalogger')
    ax2.set_xlabel('Temps (min)')
    ax2.set_ylabel('Charge (%)')
    ax2.grid()

    canvas1 = FigureCanvasTkAgg(fig1, master=fenetre)
    canvas1.draw()
    canvas1.get_tk_widget().grid(row=6, column=0)
    
    canvas2 = FigureCanvasTkAgg(fig2, master=fenetre)
    canvas2.draw()
    canvas2.get_tk_widget().grid(row=6, column=1)
    autonomie = Charge_powerbank*temps_min/(Charge_powerbank-Charge[-1])
    
    jours = int(autonomie // (24 * 60))

    # Calculer le nombre d'heures restantes
    heures_restantes = int((autonomie % (24 * 60)) // 60)

    # Calculer le nombre de minutes restantes
    minutes_restantes = int((autonomie % (24 * 60)) % 60)
    label_resultat.config(text="Autonomie estimée : " + str(jours)+ " j " + str(heures_restantes)+ " h " + str(minutes_restantes)+ " min" )

# Création de la fenêtre principale Tkinter
fenetre = tk.Tk()
fenetre.title("Autonomie du Datalogger")

# Création des widgets Tkinter
label_depart = tk.Label(fenetre, text="Trajet :",font=("Normal", 12))
label_depart.grid(row=0, column=0)


choix_trajet = tk.StringVar()
combo_depart = tk.OptionMenu(fenetre, choix_trajet, "Lyon-Grenoble", "Chambery-Modane", "Lyon-Geneve")
combo_depart.config(font=("Normal",13))  # Appliquer la police au menu déroulant
combo_depart.grid(row=0, column=1)

label_chrg = tk.Label(fenetre, text="Charge de la batterie (Ah):",font=("Normal", 12))
label_chrg.grid(row=1, column=0)
charge_powerbank = tk.Entry(fenetre)
charge_powerbank.grid(row=1,column=1)
charge_powerbank.insert(0, "20")

bouton_tracer = tk.Button(fenetre, text="Calculer", command=tracer_courbes, font=("Normal", 12))
bouton_tracer.grid(row=2, columnspan=2)

label_resultat = tk.Label(fenetre, text="",font=("Normal", 12))
label_resultat.grid(row=3,column = 0)


# Lancement de la boucle principale Tkinter
fenetre.mainloop()
