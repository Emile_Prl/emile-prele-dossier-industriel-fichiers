# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 11:08:33 2024

@author: eprel

Arduino mega : 70 mA
Arduino uno : 50 mA
MSP430 : moins 1µA en attente

https://docs.arduino.cc/learn/electronics/low-power/

"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
plt.rcParams["font.family"] = "Times New Roman"

Charge_powerbank = 20 #Ah
Arduino_conso_sleep = 6.92e-3
Arduino_conso_busy = 23.5e-3

ma200_miniflex = 5.23e-3 # sous 9V

mosfet = 77e-3
led_red = 6.41e-3
led_green = 6.9e-3
dataq = 160e-3
rendement_boost = 0.93
boost_led = 25.7e-3

flag = 0 
# Open Circuit Voltage cell characteristic
SOE = np.linspace(0,1,21)

OCV = [3.2907142857 , 3.3357142857 , 3.3715 , 3.4364285714 \
       , 3.5071428571 , 3.5481428571 , 3.581 , 3.6057857143 ,\
           3.624 , 3.6492857143 , 3.6742857143 ,3.6927857143 ,\
              3.7135714286 , 3.7678571429 , 3.8113571429 ,\
        3.8452857143 ,3.8885714286 , 3.9228571429 ,\
            3.9485714286 , 3.9929285714 , 4.0377142857] 
# function of ocv
SOE = np.linspace(0,1,len(OCV))
f_ocv = interp1d(SOE,OCV,kind = 'linear', fill_value="extrapolate") 
#ocv = f_ocv(soe) # Out of Charge Voltage at the initial soe

#%% Durée en mode veille
def autonomie_veille():
    duree_max = 50
    Charge = np.ones((duree_max*24*60))*Charge_powerbank
    duree_record = 0
    
    Current = np.zeros((duree_max*24*60))
    j=0
    i=0
    while Charge[i] > 0:
        i=i+1
        j=j+1
        soe = Charge[i-1]/Charge_powerbank
        ocv = f_ocv(soe)
        ki = max(OCV)/ocv
        
        if j%5 !=0 :
            Current[i] = (Arduino_conso_sleep+led_green)
        elif j%5 == 0 :
            Current[i] = (1*(Arduino_conso_busy+mosfet+led_green+(ma200_miniflex*9/5/rendement_boost)+boost_led)+5*(Arduino_conso_sleep+led_green))/6
            j=0
        Charge[i] = Charge[i-1]-ki*Current[i]/60
    
    Charge = Charge[:i]
    Current = Current[:i]
    duree = len(Charge)
    Time = np.linspace(1,duree/24/60,duree)
    print("Autonomie en veille : ",len(Time)/24/60," jours")
    return [Charge, Current*1000, Time]

[Charge_veille, Current_veille, Time_veille] = autonomie_veille()

#%% Durée en mode record

def autonomie_record():
    duree_max = 50
    Charge = np.ones((duree_max*24*60))*Charge_powerbank    
    Current = np.zeros((duree_max*24*60))
    i=0
    while Charge[i] > 0:
        i=i+1
        soe = Charge[i-1]/Charge_powerbank
        ocv = f_ocv(soe)
        ki = max(OCV)/ocv
        Current[i] = (Arduino_conso_busy+led_red+2*mosfet+dataq+2*(ma200_miniflex*9/5/rendement_boost)+boost_led)
        Charge[i] = Charge[i-1]-ki*Current[i]/60
    
    Charge = Charge[:i]
    Current = Current[:i]
    duree = len(Charge)
    Time = np.linspace(1,duree/24/60,duree)
    print("Autonomie en record : ",len(Time)/24/60," jours")
    return [Charge, Current*1000, Time]

[Charge_record, Current_record, Time_record] = autonomie_record()

#%% Durée en mode armé

def autonomie_armed(pmax=0.999):
    duree_max = 40
    Charge = np.ones((duree_max*24*60))*Charge_powerbank
    duree_record = 0
    
    Current = np.zeros((duree_max*24*60))
    j=0
    i=0
    while Charge[i] > 0:
        soe = Charge[i-1]/Charge_powerbank
        ocv = f_ocv(soe)
        ki = max(OCV)/ocv
        i=i+1
        j=j+1
        if j%5 !=0 :
            Current[i] = (Arduino_conso_sleep+led_green)
        elif j%5 == 0 :
            seuil = np.random.choice((0,1), p=[pmax, 1-pmax])
            if seuil==1:
                Duree_montee = np.random.randint(100,220)
                duree_record = duree_record+Duree_montee
                for k in range(Duree_montee):
                    n = k+i
                    soe = Charge[n-1]/Charge_powerbank
                    ocv = f_ocv(soe)
                    ki = max(OCV)/ocv
                    Current[n]=(Arduino_conso_busy+led_red+2*mosfet+dataq+2*(ma200_miniflex*9/5/rendement_boost)+boost_led)
                    Charge[n] = Charge[n-1]-ki*Current[i]/60
                    if Charge[n]<=0:
                        Charge[n]=0
                        break
                i=n
            Current[i] = (1*(Arduino_conso_busy+mosfet+led_green+(ma200_miniflex*9/5/rendement_boost)+boost_led)+5*(Arduino_conso_sleep+led_green))/6
            j=0
        Charge[i] = Charge[i-1]-ki*Current[i]/60
    
    Charge = Charge[:i]
    Current = Current[:i]
    duree = len(Charge)
    Time = np.linspace(1,duree/24/60,duree)
    print("Autonomie en armé : ",len(Time)/24/60," jours")
    
    print("durée d'enregistrement : ", duree_record/60)
    return [Charge, Current*1000, Time, duree]

#%% Plots 

[Charge_armed, Current_armed, Time_armed, autonomie] = autonomie_armed()

fig, ax = plt.subplots(2,figsize=(8, 3),dpi=300)
fig.suptitle("Prévision de l'autonomie du Datalogger")
ax[0].plot(Time_veille, Charge_veille, label = 'Veille')
ax[0].plot(Time_armed, Charge_armed, label = 'Armé')
ax[0].plot(Time_record, Charge_record,'r', label = 'Record')
ax[0].legend()
plt.setp(ax[0].get_xticklabels(), visible=False)
ax[0].set(ylabel="Charge (Ah)")
ax[1].plot(Time_veille, Current_veille)
ax[1].plot(Time_armed,Current_armed,'-',linewidth=0.5)
ax[1].plot(Time_record, Current_record, 'r')
ax[1].set(xlabel="Temps (jours)", ylabel="Courant (mA)")

plt.savefig('plot_estim_autonomie.jpg',bbox_inches='tight')
plt.savefig('plot_estim_autonomie.pdf',bbox_inches='tight')
plt.show()


# #%% 
# N=500
# autonomie_list = np.zeros((N))
# autonomie = 0
# for n in range(0,N):
#     print(n)
#     [Charge, Current, Time, autonomie] = autonomie_armed()
#     autonomie_list[n] = autonomie/24/60
    
# #%% plot histogram
# plt.figure(dpi=300)
# bins = np.arange(20,30,0.1)
# plt.hist(autonomie_list,bins=bins)
# plt.show()


    

